<?php

function connect($banco="phpoo",$usuario="root",$senha="",$hostname="localhost"){
    //criar conexao
    $connect= mysqli_connect($hostname, $usuario, $senha, $banco);
    //Conseguiu conectar
    if(!$connect){
        die (trigger_error("Não foi Possivel estabelecer a conexão"));
        return false;
    }else{
        //Tenta selecionar o banco de dados
        $db= $connect;
        //conseguiu selecionar o banco
        if(!$db){
            die(trigger_error("Não foi possivel selecionar o banco de dados"));
            return false;
        }else{
            return $connect;
        }
    }
}
